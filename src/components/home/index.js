import React, { useState } from 'react';
import Product from './product';
import products from './data';
import TotalMoney from './total-money';
import UnlockProduct from './unlock-product';

const Home = () => {
  const [money, setMoney] = useState(0);

  return (
    <div>
      <TotalMoney money={money} />
      {products
      // sélectionne seulement les produits débloqués
        .filter((product) => product.unlock)
      // Affichage de tous les produits débloqués
        .map((product) => (
          <Product item={product} money={money} setMoney={setMoney} />))}
      <UnlockProduct
        money={money}
        setMoney={setMoney}
        products={products}
      />
    </div>
  );
};

export default Home;
