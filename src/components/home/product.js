import React, { useState } from 'react';

const Product = ({
  item, money, setMoney
}) => {
  const [price, setPrice] = useState(item.price);
  const [total, setTotal] = useState(item.total);
  const [upgrade, setUpgrade] = useState(item.upgrade);
  const [upgradePrice, setUpgradePrice] = useState(item.upgradePrice);

  const {
    name, image
  } = item;

  const addMoney = () => {
    setMoney(money + price);
    setTotal(total + price);
  };

  const upgradeItem = () => {
    if (money > upgradePrice) {
      setMoney(money - upgradePrice);
      setUpgrade(upgrade + 1);
      setUpgradePrice(upgradePrice * (upgrade + 1));
      setPrice(price + 1);
    }
  };

  return (
    <div className="row sm-12">
      <div className="col sm-2">
        <button type="button" onClick={addMoney}>
          <figure>
            <img
              src={image}
              className="shadow sm-6 rounded-circle w-10"
              alt={name}
            />
            <figcaption>{`${price}$`}</figcaption>
          </figure>
        </button>
      </div>
      <div className="col sm-12">
        <div className="row sm-4">
          <div className="alert alert-info" role="alert">
            {`${total}$`}
          </div>
        </div>
        <div className="row sm-4">
          <progress now={60} />
        </div>
        <div className="row sm-4">
          <button type="button" className="btn" onClick={upgradeItem}>
            <div className="alert alert-warning" role="alert">
              {`${upgrade}X: ${upgradePrice}$`}
            </div>
          </button>
        </div>
      </div>
    </div>

  );
};

export default Product;
