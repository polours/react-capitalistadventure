const products = [
  {
    unlock: true,
    name: 'cog',
    image: 'https://image.flaticon.com/icons/png/128/76/76716.png',
    price: 1,
    total: 0,
    upgrade: 1,
    upgradePrice: 2.5,
    unlockPrice: 0
  },
  {
    unlock: false,
    name: 'motor',
    image: 'https://image.flaticon.com/icons/png/128/3211/3211760.png',
    price: 10,
    total: 0,
    upgrade: 1,
    upgradePrice: 25,
    unlockPrice: 50
  },
  {
    unlock: false,
    name: 'car',
    image: 'https://image.flaticon.com/icons/png/128/2736/2736918.png',
    price: 500,
    total: 0,
    upgrade: 1,
    upgradePrice: 250,
    unlockPrice: 1000
  }
];

export default products;
