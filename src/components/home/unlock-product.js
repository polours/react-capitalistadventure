import React from 'react';

const UnlockProduct = ({
  money, setMoney, products
}) => {
  let item = products.find((p) => !p.unlock);

  const unlockItem = () => {
    if (money < item.unlockPrice) {
      return;
    }
    setMoney(money - item.unlockPrice);
    item.unlock = true;
    item = products.find((p) => !p.unlock);
  };

  if (item) {
    return (
      <div className="row sm-4">
        <button type="button" className="btn" onClick={unlockItem}>
          <div className="alert alert-warning" role="alert">
            {`Unlock next item ${item.name} for ${item.unlockPrice}`}
          </div>
        </button>
      </div>
    );
  }
  return (
    <div className="d-grid gap-2">
      <button className="btn btn-primary" type="button">Tous les produits ont été débloqués</button>
    </div>
  );
};

export default UnlockProduct;
