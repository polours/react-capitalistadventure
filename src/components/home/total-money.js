import React from 'react';

const TotalMoney = ({ money }) => (
  <div className="row text-success">
    <h1 className="col border ">
      {`${money}$`}
    </h1>
  </div>
);

export default TotalMoney;
